/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  TextInput,
  Button,
  Slider
} from 'react-native';
import { Provider, connect } from 'react-redux';

import MainNavigator from './navigation';
import store from './store';

export default class App extends Component<{}> {

  render() {
    return (
      <Provider store={store}>
         <MainNavigator />
      </Provider>
    );
  }
}
