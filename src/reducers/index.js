//import {} from '../actions/types';
import { combineReducers } from 'redux';

import SearchReducer from './SearchReducer';

/*
export const INITIAL_STATE = {
};

export default ( state=INITIAL_STATE, action ) => {
	switch (action.type) {
		case LOAD_SEARCH_HISTORY_ITEMS:
		    return { ...state, search_history_items: action.payload};
		case SEARCHING:
		    return { ...state, is_searching: action.payload }
		case CHANGE_SEARCH_TEXT:
		    return { ...state, search_text: action.payload }
		default:
			return state;

	}
};*/

const rootReducer = combineReducers({
    SearchReducer
})

export default rootReducer;
