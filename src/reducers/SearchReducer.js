import {
  CHANGE_SEARCH_TEXT,
  ITEMS_IN_ROW
} from '../actions/types';

export const INITIAL_STATE = {
    search_history_items: [],
  	is_searching: false,
  	search_text: '',
    item_in_row: 0
};

export default ( state=INITIAL_STATE, action ) => {
  switch (action.type) {
    case ITEMS_IN_ROW:
      return {...state, item_in_row: action.payload};
    case CHANGE_SEARCH_TEXT:
      return {...state, search_text: action.payload};
    default:
      return state;
  }
};
