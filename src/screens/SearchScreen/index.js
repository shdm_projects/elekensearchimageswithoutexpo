import React, {Component} from 'react';
import {
  View,
  TextInput,
  Slider,
  Text,
  Button,
  StyleSheet,
  AsyncStorage,
  Platform
} from 'react-native';

import { connect } from 'react-redux';


import * as actions from '../../actions';



class SearchScreen extends Component {

  static navigationOptions = { header: null };

  popIfExists() {
    if (navigator.getCurrentIndex() > 0) {
      navigator.pop()
      return true // do not exit app
    } else {
      return false // exit app
    }
  }

  render(){
    console.log(this.props);
    return (
      <View style={styles.container}>
        <View style={styles.box}>
          <View style={styles.table}>
            <View style={styles.text}>
              <Text>Search Term:</Text>
            </View>
            <View style={styles.textInutView}>
              <TextInput
                style={styles.textInut}
                onChangeText={(text) => this.props.changeSearchText(text)}
                value={this.props.search_text}
              />
            </View>
          </View>
          <View style={styles.table2}>
            <View style={styles.text}>
              <Text>Columns:</Text>
            </View>
            <View style={styles.sliderView}>
              <Slider
                minimumValue={0}
                maximumValue={5}
                step={1}
                value={this.props.columns}
                onValueChange={(value) => this.props.changeItemInRow(value)}
                style={styles.slider}
                trackStyle={styles.sliderTrack}
                thumbStyle={styles.sliderThumb} />
            </View>
            <View style={{flex:0.3, justifyContent: 'center', marginLeft: 10}}>
              <Text>{this.props.columns}</Text>
            </View>
          </View>
          <View style={{flex:1, paddingTop: 30}}>
            <Button title="Search" onPress={() => { this.props.navigation.navigate('Result')}} />
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'white',
  },
  box: {
    flex:0.4,
    padding: 40,
    alignItems: 'center',
    justifyContent: 'center'
  },
  table: {
    flex:1,
    flexDirection: 'row'
  },
  table2: {
    flex:1,
    flexDirection: 'row'
  },
  text: {
    flex:0.7,
    justifyContent:'center',
    alignItems: 'center'
  },
  textInut: {
    height: 40,
    borderBottomColor: 'grey',
    borderBottomWidth: Platform.OS === 'ios' ? 2 : 0,
    textAlign: 'center'
  },
  textInutView: {
    flex:1,
    justifyContent: 'center'
  },
  sliderView: {
    flex:1,
    justifyContent: 'center'
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});

const mapStateToProps = (state, props) => {
	return {
    columns: state.SearchReducer.item_in_row,
    search_text: state.SearchReducer.search_text
	};
}

export default connect(mapStateToProps, actions)(SearchScreen);
