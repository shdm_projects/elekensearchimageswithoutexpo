import React, {Component} from 'react';
import {View, ListView, Text, StyleSheet, Image, ActivityIndicator, Button, TouchableHighlight, AsyncStorage} from 'react-native';
import GridList from 'react-native-grid-list';
import { connect } from 'react-redux';
import BackImage from '../../assets/back_button.png';

import * as actions from '../../actions';

const DomParser = require('react-native-html-parser').DOMParser;


class ResultScreen extends Component {

  static navigationOptions = ({ navigation, screenProps }) => ({
    header: null
  });

  constructor() {
    super();
    this.state = {
      isLoading: true,
      data: []
    }
  }

  async saveData(key, item) {
    try {
      var data = {'data': item};
      await AsyncStorage.setItem(key, JSON.stringify(data));
    } catch (error) {}
  }

  async getData(key){
    try {
      const value = await AsyncStorage.getItem(key);
      if (value !== null){
        var value = JSON.parse(value);
        console.log('Load local data...');
        let doc = new DomParser().parseFromString(value.data,'text/html');
        this.parseHtml(doc.querySelect('img'));
      }else{
        this.fetchData(key);
      }
    } catch (error) {
      this.fetchData(key);
    }

  }

  fetchData(url){
    console.log("fetching data...");
    return fetch(url)
      .then((response) => {
        this.saveData(url, response._bodyText);

        let doc = new DomParser().parseFromString(response._bodyText,'text/html');
        this.parseHtml(doc.querySelect('img'));


      })
      .catch((error) => {
        console.error(error);
      });
  }

  parseHtml(doc){
    var data = [];
    for (var i = 0; i < doc.length; i++) {
      for (var a = 0; a < doc[i].attributes.length; a++) {
        if(doc[i].attributes[a].nodeName == 'src'){
          if(doc[i].attributes[a].nodeValue.match('http')){
              data.push(doc[i].attributes[a].nodeValue);
          }
        }
      }
    }
    this.checkImageExist(data);
  }

  checkImageExist(data){
    _this = this;
    var checkedData = [];
    data.map(function(item, index){
      fetch(item)
        .then((response) => {
          if(response.status == 200){
            checkedData.push(item);
          }
          if(index == data.length - 1){
            _this.setState({
              isLoading: false,
              columns: _this.props.columns,
              data: checkedData
            });
          }
        })
        .catch((error) => {
          console.error(error);
        });
    });

  }

  componentDidMount() {
    let url = 'https://www.bing.com/images/search?q='+this.props.search_text;
    this.getData(url);
  }

  renderItemAnimation( item, animation ){
      return <View style={{flex:1, padding: 5}}><Image
        style={styles.image}
        source={{uri:item.item}}
        onLoad={() => {
          try {
            item.animation.start()
          } catch (error) {}
          }}
      /></View>;
  }

  _onPressButton(){
    this.props.navigation.navigate('Search');
  }

  render(){
    if (this.state.isLoading) {
      return (
        <View style={styles.container}>
          <View style={styles.activityIndicatorView}>
            <ActivityIndicator size="large" color="#0000ff"/>
          </View>
        </View>
      );
    }else{
      if(this.state.data.length != 0){
        return(

          <View style={styles.container}>
            <GridList
               showAnimation
               data={this.state.data}
               numColumns={this.state.columns}
               renderItem={this.renderItemAnimation}
             />
             <View style={styles.backButtonView}>
               <TouchableHighlight onPress={this._onPressButton.bind(this)}>
                 <Image
                   style={styles.backButton}
                   source={BackImage}
                 />
               </TouchableHighlight>
             </View>
          </View>

        );
      }else{
        return (
          <View style={styles.container}>
            <View style={{flex:1, justifyContent: 'center', alignItems: 'center'}}>
              <Text>
                Please write search query in previous screen
              </Text>
            </View>
            <View style={styles.backButtonView}>
              <TouchableHighlight onPress={this._onPressButton.bind(this)}>
                <Image
                  style={styles.backButton}
                  source={BackImage}
                />
              </TouchableHighlight>
            </View>
          </View>
        );
      }

    }
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 20,
    backgroundColor: 'white',
  },
  image: {
    flex:1,
    padding: 5,
    borderRadius: 1
  },
  backButton: {
    width: 30,
    height: 30
  },
  backButtonView: {
    position: 'absolute',
    top: 25,
    left: 5
  },
  activityIndicatorView: {
    flex: 1,
    justifyContent: 'center'
  }
});

const mapStateToProps = (state, props) => {
	return {
    columns: state.SearchReducer.item_in_row,
    search_text: state.SearchReducer.search_text
	};
}

export default connect(mapStateToProps, actions)(ResultScreen);
