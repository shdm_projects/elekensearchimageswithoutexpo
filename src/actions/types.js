export const LOAD_SEARCH_HISTORY_ITEMS = 'load_search_history_items';
export const SEARCHING = 'searching';
export const CHANGE_SEARCH_TEXT = 'change_search_text';
export const ITEMS_IN_ROW = 'items_in_row';
