import { AsyncStorage } from 'react-native';

import {
  CHANGE_SEARCH_TEXT,
  ITEMS_IN_ROW
} from './types';

import { INITIAL_STATE } from '../reducers/SearchReducer';

export const changeSearchText = (search = "") => {
	if (search !== null){
		return {
			type: CHANGE_SEARCH_TEXT,
			payload: search
		};
	}
	return {
		type: CHANGE_SEARCH_TEXT,
		payload: INITIAL_STATE.search_text
	};
}

export const changeItemInRow = (number = 0) => {
  return { type: ITEMS_IN_ROW, payload: number}
}
